const admin = require('firebase-admin');


module.exports = (req, res) => admin
    .firestore()
    .collection('emailAddresses')
    .doc(req.body.emailAddress)
    .set({})
    .then(() => res.status(200).send('Success'))
    .catch((e) => res.status(422).send(e.message));